
const express = require("express");
const app = express();
const PORT = process.env.PORT || 5000;
app.get("/", (req, res) => {
  res.json({ "user": "pipeline added" });
});

app.get("/start", (req, res) => {
  res.json({ "user": "Application running succesfully",PORT });
});

app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});